﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

using Adviva;
using Adviva.Osrx;

namespace AdvivaOsrxExample
{
    class MainClass
    {
        private static AdvivaConnector connector;

        public static void Main(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("  program.exe <url> <client> <clientId> <secret>");
            }
            else
            {
                // pass command line arguments to the AdvivaConnector constructor to identify client
                AdvivaConnectorFactory acf = new AdvivaConnectorFactory();
                connector = acf.CreateInstance(args[0], args[1], args[2], args[3]) as AdvivaConnector;

                // An OSRX ReconciliationRequest Object is constructed to map your data to the correct format
                ReconciliationRequest request = new SampleReconciliationRequest();

                Stage(request).Wait();
                Show(request.EhrReferenceId).Wait();
            }
        }

        private static async Task Stage(ReconciliationRequest request)
        {
            
            AdvivaOsrxFactory aof = new AdvivaOsrxFactory();
            AdvivaOsrx osrx = aof.CreateInstance(connector) as AdvivaOsrx;

            try
            {
                await osrx.Reconcile(request);
            }
            catch (ValidationException ex)
            {
                List<string> errors = ex.Data["errors"] as List<string>;
                foreach (string error in errors)
                {
                    Console.WriteLine(error);
                }
                throw new Exception();
            }
        }

        private static async Task Show(String ehrReferenceId)
        {
            AdvivaOsrx osrx = new AdvivaOsrx(connector);

            // open a browser window with the OSRX application for the given ehrReferenceId
            await osrx.Open(ehrReferenceId);

            // the SSO URL can also be obtained but as the SSO token has an expiration 
            // this should not be used except at the time of opening
            // Uri advivaUrl = await osrx.Uri(ehrReferenceId);
        }
    }
}
