﻿using System.Runtime.InteropServices;

namespace Adviva.Osrx
{
    [Guid("7e898893-f1f1-4930-b50c-55d57133e11e"), ComVisible(true)]
    public enum MedicationSource
    {
        NULL,
        PHARMACY,
        PDMP,
        PBM,
        EHR
    }
}
