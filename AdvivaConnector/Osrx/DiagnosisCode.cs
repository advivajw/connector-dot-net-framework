﻿using System.Runtime.InteropServices;

namespace Adviva.Osrx
{
    [Guid("4a4bdb21-6da5-4bf9-95fe-463ddb831792"), ComVisible(true)]
    public enum DiagnosisCode
    {
        NULL,
        ICD9,
        ICD10,
        SNOMED
    }
}