﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Adviva.Validation;

namespace Adviva.Osrx
{
    [Guid("4a9a064c-7e0c-455e-aca7-1ac0fab508fa"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [DataContract]
    public class ReconciliationRequest : Validatable
    {
        public ReconciliationRequest()
        {
        }

        [DataMember(Name = "ehrReferenceId", IsRequired = true, EmitDefaultValue = false)]
        public string EhrReferenceId;

        [DataMember(Name = "runDate", EmitDefaultValue = false)]
        public DateTime RunDate;

        [DataMember(Name = "patient", IsRequired = true, EmitDefaultValue = false)]
        public Patient Patient;

        [DataMember(Name = "diagnoses", EmitDefaultValue = false)]
        public List<Diagnosis> Diagnoses;

        [DataMember(Name = "medications", IsRequired = true, EmitDefaultValue = false)]
        public List<Medication> Medications;

        protected override List<string> Validations()
        {
            var errors = new List<string>();

            if (String.IsNullOrWhiteSpace(EhrReferenceId)) 
            {
                errors.Add("EhrReferenceId must not be null or empty");
            }

            if (Patient == null) 
            {
                errors.Add("Patient is required");
            }

            if (Medications == null || Medications.Count == 0)
            {
                errors.Add("Medications  must not be null or empty");
            }

            return errors;
        }
    }
}