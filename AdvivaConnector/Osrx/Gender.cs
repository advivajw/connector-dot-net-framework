﻿using System.Runtime.InteropServices;

namespace Adviva.Osrx
{
    [Guid("b07c9ecb-b8d9-407e-bbe0-97ee9555643a"), ComVisible(true)]
    public enum Gender
    {
        NULL,
        M,
        F,
        U
    }
}
