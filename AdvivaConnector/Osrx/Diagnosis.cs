﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Adviva.Validation;

namespace Adviva.Osrx
{
    [Guid("b58831ef-1c77-4e41-91f7-7b94d31a9a03"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [DataContract]
    public class Diagnosis : Validatable
    {
        [DataMember(Name = "externalId", EmitDefaultValue = false)]
        public string ExternalId;

        [IgnoreDataMember]
        public DiagnosisCode CodeType
        {
            get => (DiagnosisCode)Enum.Parse(typeof(DiagnosisCode), _codeType);
            set => _codeType = value.ToString("G");
        }

        [DataMember(Name = "codeType", EmitDefaultValue = false)]
        private string _codeType;

        [DataMember(Name = "code", IsRequired = true, EmitDefaultValue = false)]
        public string Code;

        [DataMember(Name = "description", IsRequired = true, EmitDefaultValue = false)]
        public string Description;

        protected override List<string> Validations()
        {
            var errors = new List<string>();

            if (String.IsNullOrWhiteSpace(Code))
            {
                errors.Add("Code must not be null or empty");
            }

            if (String.IsNullOrWhiteSpace(Description))
            {
                errors.Add("Description must not be null or empty");
            }

            return errors;
        }
    }
}
