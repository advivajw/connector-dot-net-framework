﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Adviva.Validation;

namespace Adviva.Osrx
{
    [Guid("3c82b44e-ba20-4bf0-ad8b-ed7aea3fd131"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [DataContract]
    public class Patient : Validatable
    {
        [DataMember(Name = "externalId", EmitDefaultValue = false)]
        public string ExternalId;

        [DataMember(Name = "firstName", EmitDefaultValue = false)]
        public string FirstName;

        [DataMember(Name = "lastName", EmitDefaultValue = false)]
        public string LastName;

        [DataMember(Name = "age", EmitDefaultValue = false)]
        public int Age;

        [DataMember(Name = "birthDate", EmitDefaultValue = false)]
        public DateTime BirthDate; 

        [IgnoreDataMember]
        public Gender Gender
        {
            get => (Gender)Enum.Parse(typeof(Gender), _gender);
            set => _gender = value.ToString("G");
        }

        [DataMember(Name = "gender", IsRequired  = true, EmitDefaultValue = false)]
        private string _gender;


        [DataMember(Name = "mrn", EmitDefaultValue = false)]
        public string Mrn;


        protected override List<string> Validations()
        {
            var errors = new List<string>();

            if (Gender == Gender.NULL)
            {
                errors.Add("Gender is required");
            }

            if (Age == 0 || BirthDate == DateTime.MinValue)
            {
                errors.Add("Age or BirthDate is required");
            }

            return errors;
        }
    }
}
