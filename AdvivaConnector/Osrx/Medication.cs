﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Adviva.Validation;

namespace Adviva.Osrx
{
    [Guid("590d30d2-e177-4932-a1cd-13307efe7a3e"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    [DataContract]
    public class Medication : Validatable
    {
        [DataMember(Name = "externalId", EmitDefaultValue = false)]
        public string ExternalId;

        [DataMember(Name = "source", IsRequired = true, EmitDefaultValue = false)]
        public string Source;

        [IgnoreDataMember]
        public MedicationSource SourceType
        {
            get => (MedicationSource)Enum.Parse(typeof(MedicationSource), _sourceType);
            set => _sourceType = value.ToString("G");
        }

        [DataMember(Name = "sourceType", IsRequired = true, EmitDefaultValue = false)]
        private string _sourceType;

        [DataMember(Name = "ncpdpId", IsRequired = true, EmitDefaultValue = false)]
        public string NcpdpId;

        [DataMember(Name = "pharmacyPhone", EmitDefaultValue = false)]
        public string PharmacyPhone;

        [DataMember(Name = "prescriberId", EmitDefaultValue = false)]
        public string PrescriberId;

        [DataMember(Name = "prescriberLastName", EmitDefaultValue = false)]
        public string PrescriberLastName;

        [DataMember(Name = "prescriberFirstName", EmitDefaultValue = false)]
        public string PrescriberFirstName;

        [DataMember(Name = "prescriberPhone", EmitDefaultValue = false)]
        public string PrescriberPhone;

        [DataMember(Name = "prescriberFax", EmitDefaultValue = false)]
        public string PrescriberFax;

        [DataMember(Name = "prescriptionNumber", EmitDefaultValue = false)]
        public string PrescriptionNumber;

        [DataMember(Name = "fillNumber", EmitDefaultValue = false)]
        public int FillNumber = -1;

        [DataMember(Name = "ndc11", IsRequired = true, EmitDefaultValue = false)]
        public string Ndc11;

        [DataMember(Name = "drug", IsRequired = true, EmitDefaultValue = false)]
        public string Drug;

        [DataMember(Name = "quantity", IsRequired = true, EmitDefaultValue = false)]
        public int Quantity = -1;

        [DataMember(Name = "qualifier")]
        public string Qualifier = "";

        [DataMember(Name = "daysSupply", IsRequired = true, EmitDefaultValue = false)]
        public int DaysSupply = -1;

        [DataMember(Name = "sig")]
        public string Sig = "";

        [DataMember(Name = "refillsAuthorized", IsRequired = true, EmitDefaultValue = false)]
        public int RefillsAuthorized = -1;

        [DataMember(Name = "refillsRemaining")]
        public int RefillsRemaining = 0;

        [DataMember(Name = "prescribedOn", EmitDefaultValue = false)]
        public DateTime PrescribedOn;

        [DataMember(Name = "filledOn", EmitDefaultValue = false)]
        public DateTime FilledOn;

        [DataMember(Name = "adjudicatedOn", EmitDefaultValue = false)]
        public DateTime AdjudicatedOn;

        protected override List<string> Validations()
        {
            var errors = new List<string>();

            if (String.IsNullOrWhiteSpace(Source))
            {
                errors.Add("Source must not be null or empty");
            }

            if (SourceType == MedicationSource.NULL)
            {
                errors.Add("SourceType is required");
            }

            if (String.IsNullOrWhiteSpace(NcpdpId))
            {
                errors.Add("NcpdpId must not be null or empty");
            }

            if (String.IsNullOrWhiteSpace(PrescriberId))
            {
                errors.Add("PrescriberId must not be null or empty");
            }

            if (FillNumber < 0)
            {
                errors.Add("FillNumber is required");
            }

            if (String.IsNullOrWhiteSpace(Ndc11))
            {
                errors.Add("Ndc11 must not be null or empty");
            }

            if (String.IsNullOrWhiteSpace(Drug))
            {
                errors.Add("Drug must not be null or empty");
            }

            if (Quantity < 0)
            {
                errors.Add("Quantity is required");
            }

            if (DaysSupply < 0)
            {
                errors.Add("DaysSupply is required");
            }

            if (PrescribedOn == DateTime.MinValue && SourceType == MedicationSource.EHR)
            {
                errors.Add("PrescribedOn is required for EHR records");
            }

            if (FilledOn == DateTime.MinValue && (SourceType == MedicationSource.PBM || SourceType == MedicationSource.PDMP))
            {
                errors.Add("FilledOn is required for PBM and PDMP records");
            }

            if (AdjudicatedOn == DateTime.MinValue && SourceType == MedicationSource.PBM)
            {
                errors.Add("AdjudicatedOn is required for EHR records");
            }

            return errors;
        }
    }
}