﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;

namespace Adviva.Validation
{
    [DataContract]
    public abstract class Validatable
    {
        public ValidatableResult Validate(bool allProperties = false) 
        {
            var errors = Validations();

            if (allProperties) 
            {
                Type type = this.GetType();
                foreach(FieldInfo info in type.GetFields())
                {
                    if (info.FieldType.IsSubclassOf(typeof(Validatable))) 
                    {
                        Validatable field = info.GetValue(this) as Validatable;
                        foreach(string error in ((field.Validate().Errors))) 
                        {
                            errors.Add(info.Name + ": " + error);
                        }
                    }
                    else if (typeof(System.Collections.ICollection).IsAssignableFrom(info.FieldType))
                    {
                        ICollection collection = info.GetValue(this) as ICollection;
                        Type collectionType = collection.GetType().GetGenericArguments()[0];

                        if (collectionType.IsSubclassOf(typeof(Validatable)))
                        {
                            int index = 0;
                            foreach (object item in collection)
                            {
                                foreach (string error in (item as Validatable).Validate().Errors)
                                {
                                    errors.Add(collectionType.Name + "[" + index + "]: " + error);
                                }
                                index++;
                            }
                        }
                    }
                }
            }

            ValidatableResult result = new ValidatableResult(errors);
            return result; 
        }

        protected abstract List<string> Validations();

        public bool IsValid()
        {
            return this.Validate().Success;
        }

        public List<string> ValidationErrors()
        {
            return this.Validate().Errors;
        }
    }

    public class ValidatableResult
    {
        public ValidatableResult(List<string> errors)
        {
            Success = errors.Count == 0;
            Errors = errors;
        }

        public bool Success { get; }

        public List<string> Errors { get; }
    }
}
