﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

using Adviva.Connector;

namespace Adviva
{
    [Guid("2603c5de-3ddd-4f6d-8bee-84b3a4e94dc7"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class AdvivaConnector
    {
        private readonly AccessCredentials credentials;
        private static readonly HttpClient client;
        private static AccessTokens tokens;

        internal Uri BaseAddress => client.BaseAddress;
        internal Uri TokenUri => new Uri("/auth/realms/OSRX/protocol/openid-connect/token");

        static AdvivaConnector() 
        {
            client = new HttpClient();
            tokens = null;
        }

        public AdvivaConnector(String url, String client, String clientId, String secret)
        {
            AdvivaConnector.client.BaseAddress = new Uri(url);
            credentials = new AccessCredentials(client, clientId, secret);
        }

        internal async Task<JwtToken> RefreshToken()
        {
            tokens = await GetTokens();
            return tokens.Refresh;
        }

        internal async Task<HttpResponseMessage> PostAsync(Uri requestUri, HttpContent content)
        {
            if (tokens == null || tokens.Access.Expired()) 
            {
                tokens = await GetTokens();
            }

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokens.Access.ToString());

            HttpResponseMessage response = await client.PostAsync(requestUri, content);
            response.EnsureSuccessStatusCode();

            return response;
        }

        private async Task<AccessTokens> GetTokens()
        {
            Dictionary<string, string> values = new Dictionary<string, string>
            {
                { "client_id", credentials.Client },
                { "grant_type", "password" },
                { "username", credentials.ClientId },
                { "password", credentials.Secret }
            };

            FormUrlEncodedContent content = new FormUrlEncodedContent(values);
            HttpResponseMessage response = await client.PostAsync(TokenUri, content);
            response.EnsureSuccessStatusCode();

            Stream stream = await response.Content.ReadAsStreamAsync();
            stream.Position = 0;

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(AccessTokens));
            tokens = (AccessTokens)ser.ReadObject(stream);
            return tokens;
        }
    }

    [Guid("bb8f2bf1-b4d8-4c01-97d5-b5bf056ac98c"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class AdvivaConnectorFactory
    {
        public AdvivaConnector CreateInstance(String url, String client, String clientId, String secret)
        {
            return new AdvivaConnector(url, client, clientId, secret);
        }
    }
}
