﻿using System;
using System.CodeDom;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;

namespace Adviva
{
    public class DateTimeDataContractSurrogate : IDataContractSurrogate
    {
        public DateTimeDataContractSurrogate()
        {
        }

        public object GetCustomDataToExport(MemberInfo memberInfo, Type dataContractType)
        {
            throw new NotImplementedException();
        }

        public object GetCustomDataToExport(Type clrType, Type dataContractType)
        {
            throw new NotImplementedException();
        }

        public Type GetDataContractType(Type type)
        {
            return type;
        }

        public object GetDeserializedObject(object obj, Type targetType)
        {
            if (targetType == typeof(DateTime))
            {
                DateTime dateTime = (DateTime)obj;
                return dateTime.ToString("yyy-MM-dd", CultureInfo.InvariantCulture);
            }
            return obj;
        }

        public void GetKnownCustomDataTypes(Collection<Type> customDataTypes)
        {
            throw new NotImplementedException();
        }

        public object GetObjectToSerialize(object obj, Type targetType)
        {
            if (targetType == typeof(DateTime))
            {
                return DateTime.ParseExact(obj.ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
            return obj;
        }

        public Type GetReferencedTypeOnImport(string typeName, string typeNamespace, object customData)
        {
            throw new NotImplementedException();
        }

        public CodeTypeDeclaration ProcessImportedType(CodeTypeDeclaration typeDeclaration, CodeCompileUnit compileUnit)
        {
            return typeDeclaration;
        }
    }
}
