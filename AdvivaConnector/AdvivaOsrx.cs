﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Adviva.Connector;
using Adviva.Osrx;
using Adviva.Validation;

namespace Adviva
{
    [Guid("d3d67500-a713-4fcb-a4a5-a6dd4f7b338f"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class AdvivaOsrx
    {
        private readonly AdvivaConnector Connector;
        private Uri ReconcileUri => new Uri("/api/medrec/rs/reconcile");

        public AdvivaOsrx(AdvivaConnector connector) 
        {
            this.Connector = connector;
        }

        public async Task<HttpResponseMessage> Reconcile(ReconciliationRequest request)
        {
            ValidatableResult validation = request.Validate(true);
            if (!validation.Success)
            {
                ValidationException exception = new ValidationException("Invalid ReconciliationRequest");
                exception.Data.Add("errors", validation.Errors);
                throw exception;
            }
            else
            {
                MemoryStream stream = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ReconciliationRequest),
                    new DataContractJsonSerializerSettings
                    {
                        DateTimeFormat = new DateTimeFormat("yyyy-MM-dd")
                    }
                );

                ser.WriteObject(stream, request);
                stream.Position = 0;
                StreamReader sr = new StreamReader(stream);
                String json = sr.ReadToEnd();
                return await Reconcile(json);
            }
        }

        public async Task Open(String ehrReferenceId)
        {
            Uri openUri =  await Uri(ehrReferenceId);
            Process browser = new Process();
            browser.StartInfo.UseShellExecute = true;
            browser.StartInfo.FileName = openUri.AbsoluteUri;
            browser.Start();
        }

        public async Task<Uri> Uri(String ehrReferenceId)
        {
            JwtToken refreshToken = await Connector.RefreshToken();
            Uri SsoUri = new Uri(String.Format("/sso?auth=jwt&ehrReferenceId={0}&token={1}", ehrReferenceId, refreshToken));
            return new Uri(Connector.BaseAddress, SsoUri);
        }

        private async Task<HttpResponseMessage> Reconcile(String json)
        {
            StringContent content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await Connector.PostAsync(ReconcileUri, content);
            response.EnsureSuccessStatusCode();
            return response;
        }

    }

    [Guid("a7703109-5bf0-4515-96c5-42f0cbc224d1"), ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class AdvivaOsrxFactory
    {
        public AdvivaOsrx CreateInstance(AdvivaConnector connector)
        {
            return new AdvivaOsrx(connector);
        }
    }
}
