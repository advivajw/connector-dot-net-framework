﻿using System;
using System.Runtime.Serialization;

namespace Adviva.Connector
{
    [DataContract]
    public class JwtTokenPayload
    {
        [DataMember]
        public string exp;
    }
}
