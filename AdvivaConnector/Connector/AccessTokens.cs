﻿using System.Runtime.Serialization;

namespace Adviva.Connector
{
    [DataContract]
    class AccessTokens
    {
        [IgnoreDataMember]
        internal JwtToken Access;

        [IgnoreDataMember]
        internal JwtToken Refresh;

        [DataMember]
        private string access_token = null;

        [DataMember]
        private string refresh_token = null;

        [OnDeserialized]
        void OnDeserialized(StreamingContext ctx)
        {
            Access = new JwtToken(access_token);
            Refresh = new JwtToken(refresh_token);
        }
    }
}