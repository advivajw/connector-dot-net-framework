﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace Adviva.Connector
{
    class JwtToken
    {
        readonly string RawToken;
        internal readonly JwtTokenPayload Payload;

        public override string ToString()
        {
            return RawToken;
        }

        internal JwtToken(String rawToken)
        {
            RawToken = rawToken;

            String[] sections = RawToken.Split('.');

            String payloadBase64Url = sections[1];
            MemoryStream payload = DecodeBase64Url(payloadBase64Url);
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(JwtTokenPayload));
            Payload = ser.ReadObject(payload) as JwtTokenPayload;
        }

        internal Boolean Expired() {
            TimeSpan EpochNow = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));
            return Payload.Expires <= EpochNow.TotalSeconds;
        }

        private MemoryStream DecodeBase64Url(String base64Url) 
        {
            String base64 = base64Url.Replace('-', '+').Replace('_', '/');

            while (base64.Length % 4 != 0) base64 += '=';
            byte[] payloadData = Convert.FromBase64String(base64);

            MemoryStream ms = new MemoryStream(payloadData);
            return ms;
        }
    }

    [DataContract]
    class JwtTokenPayload
    {
        [DataMember(Name = "exp")]
        internal long Expires = 0;
    }
}
