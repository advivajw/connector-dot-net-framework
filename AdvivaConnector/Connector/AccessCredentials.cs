﻿using System;

namespace Adviva.Connector
{
    class AccessCredentials
    {
        internal String Client;
        internal String ClientId;
        internal String Secret;

        internal AccessCredentials(String client, String clientId, String secret)
        {
            Client = client;
            ClientId = clientId;
            Secret = secret;
        }
    }
}
